"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


from re import U
from settings_global import *
import arcade
import os
import math

def zombie_boss_movement(
    boss_sprite  : arcade.AnimatedTimeBasedSprite,
    player_sprite: arcade.AnimatedWalkingSprite) -> None:
    """Movement logic for the boss
    """

    dest_x = BOSS_PATHS[boss_sprite.path_index][0]
    dest_y = BOSS_PATHS[boss_sprite.path_index][1]

    start_x = boss_sprite.center_x
    start_y = boss_sprite.center_y

    x_diff = dest_x - start_x
    y_diff = dest_y - start_y


    angle = math.atan2(y_diff, x_diff)

    distance_to_target = math.sqrt(
        (boss_sprite.center_x-dest_x)**2+
        (boss_sprite.center_y-dest_y)**2)

    speed = min(BOSS_SPEED, distance_to_target)

    boss_sprite.center_x += math.cos(angle) * speed
    boss_sprite.center_y += math.sin(angle) * speed

    distance_to_target = math.sqrt(
        (boss_sprite.center_x-dest_x)**2+
        (boss_sprite.center_y-dest_y)**2)

    if distance_to_target-10 <= BOSS_SPEED:
        boss_sprite.path_index += 1
        if boss_sprite.path_index > len(BOSS_PATHS)-1:
            boss_sprite.path_index = 0

def create_zombie_level_boss(sprite_textures: str) -> arcade.AnimatedTimeBasedSprite:
    """Create a animated walking boss sprite.

        Params:
            sprite_textures(str): Path to the sprite images

        Returns:
            boss_sprite (AnimtedWalkingSprite)
    """

    boss_sprite = arcade.AnimatedTimeBasedSprite(
        os.path.join(sprite_textures, "zombie_walk0.png"),
        image_x = 0,
        image_y = 0,
        image_width = 96,
        image_height = 128
    )

    # Walking right frames
    for i in range(1,8):
        texture = arcade.load_texture(os.path.join(sprite_textures, f"zombie_walk{i}.png"))
        boss_sprite.frames.append(arcade.AnimationKeyframe(tile_id=i, duration=25*BOSS_SPEED, texture=texture))

    # Walking left frames (mirror of the right)
    for i in range(1,8):
        texture = arcade.load_texture(os.path.join(sprite_textures, f"zombie_walk{i}.png"), mirrored=True)
        boss_sprite.frames.append(arcade.AnimationKeyframe(tile_id=i, duration=25*BOSS_SPEED, texture=texture))

    boss_sprite.center_x = BOSS_X_SPAWN
    boss_sprite.center_y = BOSS_Y_SPAWN

    #boss_sprite.scaling = BOSS_SCALING
    return boss_sprite

