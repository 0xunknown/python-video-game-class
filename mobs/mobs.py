"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import arcade
import math
import random

from typing import List
from settings_global import *

def update_sprite_movement(
    mob_sprite   : arcade.Sprite,
    player_spirte: arcade.Sprite,
    collision    : bool) -> None:
    """Update sprite location and status

        Parameters:
            mob_sprite    (arcade.Sprite): arcade sprite object. Our monster!
            player_sprite (arcade.Sprite): The player. Our hero!
            collision     (bool)         : True if a mob made contact with a object

        The goal:
            Get the mobs to move back and forth!!

        Questions to consider:
            1) How fast should the mob move?
            2) When should the mob turn around?
            3) How high should the mob jump?
            4) When should the mob jump?
    """

    # NOTE: THIS IS A PLACE HOLDER. COMMENT THIS OUT WHEN DOING
    # TASK 1 & 2

    # TODO: TASK 1
    """Make the mob move back and forth or chase the player
        Step 1): When should the mob change directions?
        Step 2): Write a IF/ELSE condition based on
                 when the mob should turn around
                 NOTE: replace random_collision_chance variable
        Step 3): How fast should the mob move?
        Step 4): How to make the mob change direction?
    """
    jump_speed = 5
    move_speed = 5
    npc_distance = 518

    if collision:
        mob_sprite.change_x = move_speed
    else:
        mob_sprite.change_x = -move_speed

    #random_collision_chance = random.randint(0,100)

    #if random_collision_chance < 50: # TODO: Fix this IF statement!
    #    mob_sprite.change_x = MOB_SPEED
    #else:
    #    mob_sprite.change_x = -MOB_SPEED

    # TODO: TASK 2: Make the mob jump
    """Make the mob jump when close to the player
        Step 1): What is the players current x & y: (x,y)
        Step 2): What is the mobs curent x & y: (x,y)?
        Step 3): What is the distance between the player and the mob?
        Step 4): Make a IF condition and compare the distance to a value
        Step 5): Update the mobs change_y speed. What is balanced?
    """

    jump_chance = random.randint(0,100)
    if npc_distance < 100:
        mob_sprite.change_y = jump_speed
    else:
        mob_sprite.change_y = -jump_speed



