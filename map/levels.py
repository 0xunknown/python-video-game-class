"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import arcade
import time
import os

from settings_global import *

class Levels():
    def __init__(self):
        """
        """
        self.tile_map       = None
        self.physics_engine = None
        self.time_limit     = None

    def build_level_1(self,
        player_sprite: arcade.Sprite,
        boss_sprite  : arcade.Sprite) -> None:
        """Create example map level: blocks/mobs/etc..
        map.json holds all information for the level

        DO NOT CHANGE THIS FUNCTION UNLESS YOU HAVE A PLAN!
        """

        # For each level you create, you must load it with the full path
        map_name             = "level_0.json"
        absolute_path_to_map = os.path.dirname(os.path.realpath(__file__))

        absolute_path_to_map = os.path.join(
            absolute_path_to_map,
            map_name)

        # Creating a level in Tile Editor
        # Layers can be assigned to each type of blocks
        # This helps you to seperate logic for each type of block
        layer_options = {
            "coins"        : {"use_spatial_hash": True},
            "ground"       : {"use_spatial_hash": True},
            "goal"         : {"use_spatial_hash": True},
            "ground_mobs"  : {"use_spatial_hash": True},
            "flying_mobs"  : {"use_spatial_hash": True},
            "traps"        : {"use_spatial_hash": True},
            "powerup"      : {"use_spatial_hash": True},
            "moving_tiles" : {"use_spatial_hash": True},
            "check_point"  : {"use_spatial_hash": True},
            "boss"         : {"use_spatial_hash": True},
            "mob_ground"   : {"use_spatial_hash": True}}


        # Read in the tiled map and save the data in a local variable
        self.tile_map = arcade.load_tilemap(
            absolute_path_to_map, layer_options=layer_options, scaling=0.5
        )

        # After readind the map data, we assign the data to a new variable
        # During the game we will delete sprites (i.e., coins)
        # Thus, we need to keep a copy for respawns or resets
        self.wall_list    = self.tile_map.sprite_lists['ground']
        self.coin_list    = self.tile_map.sprite_lists['coins']
        self.mobs         = self.tile_map.sprite_lists['ground_mobs']
        self.flying_mobs  = self.tile_map.sprite_lists['flying_mobs']
        self.goals        = self.tile_map.sprite_lists['goal']
        self.death_traps  = self.tile_map.sprite_lists['traps']
        self.powerups     = self.tile_map.sprite_lists['powerup']
        self.boss         = self.tile_map.sprite_lists['boss'][-1]
        self.moving_tiles = self.tile_map.sprite_lists['moving_tiles']
        self.check_points = self.tile_map.sprite_lists["check_point"]
        self.mob_ground   = self.tile_map.sprite_lists["mob_ground"]

        # Add physics between any two objects,
        # For now: Physics between the player and the walls/ground/obsitcles
        # Keep player from running through the wall_list layer
        walls = [self.wall_list, self.moving_tiles, self.goals, self.mob_ground, self.powerups]
        self.physics_engine = arcade.PhysicsEnginePlatformer(
            player_sprite, walls, gravity_constant=GRAVITY)

        self.mob_physics = []
        for mob in self.mobs:
            mob.collision = False
            mob_physics_engine = arcade.PhysicsEnginePlatformer(
                mob,
                walls,
                gravity_constant=GRAVITY)
            self.mob_physics.append(mob_physics_engine)

        walls = [self.wall_list, self.moving_tiles, self.mob_ground]
        self.boss_physics_engine = arcade.PhysicsEnginePlatformer(
            boss_sprite, walls, gravity_constant=GRAVITY
        )

        # How long it should take to complete the level
        self.time_limit = 300

        return self
