from re import X
import arcade
import math

from typing import Tuple

#DO NOT CHANGE (UNLESS YOU KNOW WHY)
MOVING_PLATFORM_1_POSITIONS = [
    (3000, 160),
    (3300, 160)
]

#DO NOT CHANGE (UNLESS YOU KNOW WHY)
MOVING_PLATFORM_2_POSITIONS = [
    (4600, 150),
    (4948, 308)
]

def first_moving_tile(
    moving_tile   : arcade.Sprite,
    position_index: int) -> int:
    """Make two tiles move back and forth

        Params:
            moving_tile        (arcade.Sprite): First moving platform by lava
            position_index              (int): 0 if moving to the right
                                               1 if moving to the left

        QUESTION: How can we make the floating platforms move correctly?

        NOTE: Pick two points and draw a line.
        We need to figure out the displacement and change direction when
        the displacement is zero!
    """

    # TODO Task 1:
    """Create a variable that stores the move speed of the tile
        Step 1): What is a good name for this variable?
        Step 2): The value of the variable should be a number from 0 - 100
        Step 3): What is a good speed? Is their a problem if it is too fast?
    """
    tile_speed=0.5
    # DO NOT CHANGE:
    """
        NOTE:
            We need both of them to move between two points.
            What should the second point be for each platform?

        NOTE: Draw on paper what the map looks like with both platforms
        Draw a line from the start of the platform to how far you want it to travel.
        How can we determine when it should turn around? (Move back and forth)

        x_diff is the difference between two points on the x-axis
    """
    x_diff = moving_tile.center_x - MOVING_PLATFORM_1_POSITIONS[position_index][0] 
    

    # TODO: TASK 2
    """Calculate the distance between two points first moving platform
        Step 1): What should the current_distance mean?
            - Draw on paper and discuss while looking at the game
        Step 2): Replace the 0 value with something else that represents the current distance
            - The tile is actively moving.
            - What is the current_distance value as it approaches the destination?

        What happens if the distance is negative?
        Does the different in y matter?

        NOTE: Formulas to use from python
            1) math.sqrt()
                - returns the square root value
            2) abs()
                - makes a value always be positive (absolute value)

    """
    current_distance = abs(x_diff) #TODO: Change my values

    # TODO: TASK 3
    """
        Step 1): Compare the position_index to zero and update change_x with a
                 position move_speed value

        Step 2): Compare the position_index to one and update change_x with a
                 negative move_speed value

        # NOTE: As the tile moves closer to the ending points on your line,
                the distance decreases and will adventually be zero or less
                When this happens, we want it to turn around and head back to
                our first location
    """
    if position_index == 0:
        moving_tile.change_x = tile_speed # TODO: Change to your move speed variable
    else:
        moving_tile.change_x = -tile_speed # TODO: Change to your move speed variable

    # TODO: TASK 4
    """At what current_distance value should we turn around?
        Step 1): Figure out a good minimum turnaround distance
            - As the moving tile approaches the turnaround point, what value is the current_distance?
            - The move speed matters here! The tile will change based on your speed
        Step 2): Figoure out a good maximum turnaround distance
            - What should the maximum turnaround distance be?
            - View the coorindates at the top and subtract them

        NOTE: The moving tile is going back and forth between two points
        As it reaches the destination point, the distance should approach 0

        Source (Point 0) <------moving_tile----> Destination (Point 1)
    """
    min_turnaround_distance = tile_speed # TODO: Change my value
    max_turnaround_distance = 500 # TODO: Change my value

    if current_distance <= min_turnaround_distance or current_distance > max_turnaround_distance:
        if position_index == len(MOVING_PLATFORM_1_POSITIONS)-1:
            position_index = 0
        else:
            position_index = 1

    return position_index

def second_moving_tile(
    moving_tile    : arcade.Sprite,
    position_index : int) -> int:
    """Make two tiles move back and forth

        Params:
            moving_tile  (arcade.Sprite): First moving platform by the boss
            position_index         (int): 0 if moving to the right
                                          1 if moving to the left

        QUESTION: How can we make the floating platforms move correctly?

        NOTE: Pick two points and draw a line.
        We need to figure out the displacement and change direction when
        the displacement is zero!
    """
    # TODO Task 5:
    """Create a variable that stores the move speed of the tile
        Step 1): What is a good name for this variable?
        Step 2): The value of the variable should be a number from 0 - 100
        Step 3): What is a good speed? Is their a problem if it is too fast?
    """

    #DO NOT CHANGE
    """
        NOTE:
            We need both of them to move between two points.
            What should the second point be for each platform?

        NOTE: Draw on paper what the map looks like with both platforms
        Draw a line from the start of the platform to how far you want it to travel.
        How can we determine when it should turn around? (Move back and forth)

        x_diff is the difference between two points on the x-axis
        y_diff is the difference between two points on the y-axis
    """
    x_diff = moving_tile.center_x - MOVING_PLATFORM_2_POSITIONS[position_index][0]
    y_diff = moving_tile.center_y - MOVING_PLATFORM_2_POSITIONS[position_index][1]

    # TODO: TASK 6
    """Calculate the distance between two points first moving platform
        Step 1): What formula should we use to calculate the current distance?
            - We want the tile to move diagonally
            - There is a change in x and y

        Should the current distance be negative?
        Does the different in y matter?

        NOTE: Formulas to use from python
            1) math.sqrt()
                - returns the square root value
            2) abs()
                - makes a value always be positive (absolute value)

    """

    current_distance = 0 #TODO: Change my values

    # TODO: TASK 7
    """
        Step 1): When the position_index equals 0, what should the change_x be?
            - 0 is not correct!

        Step 2): If the tile moves back and forth, how do we get it to change direction?
            - What does positive and negative speed mean?

        # NOTE: As the tile moves closer to the ending points on your line,
                the distance decreases and will adventually be zero or less
                When this happens, we want it to turn around and head back to
                our first location (i.e., change direction)
    """
    if position_index == 0:
        moving_tile.change_x = 0 # TODO: Change my value
        moving_tile.change_y = 0 # TODO: Change my value
    else:
        moving_tile.change_x = 0 # TODO: Change my value
        moving_tile.change_y = 0 # TODO: Change my value

    # TODO: TASK 8
    """At what current_distance value should we turn around?

        NOTE: The moving tile is going back and forth between two points
        As it reaches the destination point, the distance should approach 0

        Source (Point 0) <------moving_tile----> Destination (Point 1)
    """
    min_turnaround_distance = 0 #TODO: Change my value
    max_turnaround_distance = 0 #TODO: Change my value

    # DO NOT EDIT
    if current_distance <= min_turnaround_distance or current_distance > max_turnaround_distance:
        if position_index == len(MOVING_PLATFORM_2_POSITIONS)-1:
            position_index = 0
        else:
            position_index = 1

    return position_index

