<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.8.4" name="video_game_project" tilewidth="352" tileheight="192" tilecount="210" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="256" height="192" source="../animation/images/background/back-buildings.png"/>
 </tile>
 <tile id="1">
  <image width="256" height="192" source="../animation/images/background/far-buildings.png"/>
 </tile>
 <tile id="2">
  <image width="352" height="192" source="../animation/images/background/foreground.png"/>
 </tile>
 <tile id="3">
  <image width="128" height="128" source="../animation/images/items/coinBronze.png"/>
 </tile>
 <tile id="4">
  <image width="128" height="128" source="../animation/images/items/coinGold.png"/>
 </tile>
 <tile id="5">
  <image width="128" height="128" source="../animation/images/items/coinGold_ll.png"/>
 </tile>
 <tile id="6">
  <image width="128" height="128" source="../animation/images/items/coinGold_lr.png"/>
 </tile>
 <tile id="7">
  <image width="128" height="128" source="../animation/images/items/coinGold_ul.png"/>
 </tile>
 <tile id="8">
  <image width="128" height="128" source="../animation/images/items/coinGold_ur.png"/>
 </tile>
 <tile id="9">
  <image width="128" height="128" source="../animation/images/items/coinSilver.png"/>
 </tile>
 <tile id="10">
  <image width="64" height="64" source="../animation/images/items/coinSilver_test.png"/>
 </tile>
 <tile id="11">
  <image width="128" height="128" source="../animation/images/items/flagGreen_down.png"/>
 </tile>
 <tile id="12">
  <image width="128" height="128" source="../animation/images/items/flagGreen1.png"/>
 </tile>
 <tile id="13">
  <image width="128" height="128" source="../animation/images/items/flagGreen2.png"/>
 </tile>
 <tile id="14">
  <image width="128" height="128" source="../animation/images/items/flagRed_down.png"/>
 </tile>
 <tile id="15">
  <image width="128" height="128" source="../animation/images/items/flagRed1.png"/>
 </tile>
 <tile id="16">
  <image width="128" height="128" source="../animation/images/items/flagRed2.png"/>
 </tile>
 <tile id="17">
  <image width="128" height="128" source="../animation/images/items/flagYellow_down.png"/>
 </tile>
 <tile id="18">
  <image width="128" height="128" source="../animation/images/items/flagYellow1.png"/>
 </tile>
 <tile id="19">
  <image width="128" height="128" source="../animation/images/items/flagYellow2.png"/>
 </tile>
 <tile id="20">
  <image width="128" height="128" source="../animation/images/items/gemBlue.png"/>
 </tile>
 <tile id="21">
  <image width="128" height="128" source="../animation/images/items/gemGreen.png"/>
 </tile>
 <tile id="22">
  <image width="128" height="128" source="../animation/images/items/gemRed.png"/>
 </tile>
 <tile id="23">
  <image width="128" height="128" source="../animation/images/items/gemYellow.png"/>
 </tile>
 <tile id="24">
  <image width="64" height="64" source="../animation/images/items/gold_1.png"/>
 </tile>
 <tile id="25">
  <image width="64" height="64" source="../animation/images/items/gold_2.png"/>
 </tile>
 <tile id="26">
  <image width="64" height="64" source="../animation/images/items/gold_3.png"/>
 </tile>
 <tile id="27">
  <image width="64" height="64" source="../animation/images/items/gold_4.png"/>
 </tile>
 <tile id="28">
  <image width="128" height="128" source="../animation/images/items/keyBlue.png"/>
 </tile>
 <tile id="29">
  <image width="128" height="128" source="../animation/images/items/keyGreen.png"/>
 </tile>
 <tile id="30">
  <image width="128" height="128" source="../animation/images/items/keyRed.png"/>
 </tile>
 <tile id="31">
  <image width="128" height="128" source="../animation/images/items/keyYellow.png"/>
 </tile>
 <tile id="32">
  <image width="128" height="128" source="../animation/images/items/ladderMid.png"/>
 </tile>
 <tile id="33">
  <image width="128" height="128" source="../animation/images/items/ladderTop.png"/>
 </tile>
 <tile id="34">
  <image width="128" height="128" source="../animation/images/items/star.png"/>
 </tile>
 <tile id="35">
  <image width="128" height="128" source="../animation/images/mobs/bee.png"/>
 </tile>
 <tile id="36">
  <image width="128" height="128" source="../animation/images/mobs/fishGreen.png"/>
 </tile>
 <tile id="37">
  <image width="128" height="128" source="../animation/images/mobs/fishPink.png"/>
 </tile>
 <tile id="38">
  <image width="128" height="128" source="../animation/images/mobs/fly.png"/>
 </tile>
 <tile id="39">
  <image width="128" height="128" source="../animation/images/mobs/frog.png"/>
 </tile>
 <tile id="40">
  <image width="128" height="128" source="../animation/images/mobs/frog_move.png"/>
 </tile>
 <tile id="41">
  <image width="128" height="128" source="../animation/images/mobs/ladybug.png"/>
 </tile>
 <tile id="42">
  <image width="128" height="128" source="../animation/images/mobs/mouse.png"/>
 </tile>
 <tile id="43">
  <image width="128" height="128" source="../animation/images/mobs/saw.png"/>
 </tile>
 <tile id="44">
  <image width="128" height="128" source="../animation/images/mobs/sawHalf.png"/>
 </tile>
 <tile id="45">
  <image width="128" height="128" source="../animation/images/mobs/slimeBlock.png"/>
 </tile>
 <tile id="46">
  <image width="128" height="128" source="../animation/images/mobs/slimeBlue.png"/>
 </tile>
 <tile id="47">
  <image width="128" height="128" source="../animation/images/mobs/slimeBlue_move.png"/>
 </tile>
 <tile id="48">
  <image width="128" height="128" source="../animation/images/mobs/slimeGreen.png"/>
 </tile>
 <tile id="49">
  <image width="128" height="128" source="../animation/images/mobs/slimePurple.png"/>
 </tile>
 <tile id="50">
  <image width="128" height="128" source="../animation/images/mobs/wormGreen.png"/>
 </tile>
 <tile id="51">
  <image width="128" height="128" source="../animation/images/mobs/wormGreen_dead.png"/>
 </tile>
 <tile id="52">
  <image width="128" height="128" source="../animation/images/mobs/wormGreen_move.png"/>
 </tile>
 <tile id="53">
  <image width="128" height="128" source="../animation/images/mobs/wormPink.png"/>
 </tile>
 <tile id="54">
  <image width="54" height="9" source="../animation/images/projectiles/laserBlue01.png"/>
 </tile>
 <tile id="55">
  <image width="9" height="54" source="../animation/images/projectiles/laserRed01.png"/>
 </tile>
 <tile id="56">
  <image width="101" height="84" source="../animation/images/projectiles/meteorGrey_big1.png"/>
 </tile>
 <tile id="57">
  <image width="29" height="26" source="../animation/images/projectiles/meteorGrey_small2.png"/>
 </tile>
 <tile id="58">
  <image width="18" height="18" source="../animation/images/projectiles/meteorGrey_tiny1.png"/>
 </tile>
 <tile id="59">
  <image width="16" height="15" source="../animation/images/projectiles/meteorGrey_tiny2.png"/>
 </tile>
 <tile id="60">
  <image width="128" height="128" source="../animation/images/tiles/bomb.png"/>
 </tile>
 <tile id="61">
  <image width="128" height="128" source="../animation/images/tiles/boxCrate.png"/>
 </tile>
 <tile id="62">
  <image width="128" height="128" source="../animation/images/tiles/boxCrate_double.png"/>
 </tile>
 <tile id="63">
  <image width="128" height="128" source="../animation/images/tiles/boxCrate_single.png"/>
 </tile>
 <tile id="64">
  <image width="128" height="128" source="../animation/images/tiles/brickBrown.png"/>
 </tile>
 <tile id="65">
  <image width="128" height="128" source="../animation/images/tiles/brickGrey.png"/>
 </tile>
 <tile id="66">
  <image width="128" height="128" source="../animation/images/tiles/brickTextureWhite.png"/>
 </tile>
 <tile id="67">
  <image width="128" height="128" source="../animation/images/tiles/bridgeA.png"/>
 </tile>
 <tile id="68">
  <image width="128" height="128" source="../animation/images/tiles/bridgeB.png"/>
 </tile>
 <tile id="69">
  <image width="128" height="128" source="../animation/images/tiles/bush.png"/>
 </tile>
 <tile id="70">
  <image width="128" height="128" source="../animation/images/tiles/cactus.png"/>
 </tile>
 <tile id="71">
  <image width="128" height="128" source="../animation/images/tiles/dirt.png"/>
 </tile>
 <tile id="72">
  <image width="128" height="128" source="../animation/images/tiles/dirtCenter.png"/>
 </tile>
 <tile id="73">
  <image width="128" height="128" source="../animation/images/tiles/dirtCenter_rounded.png"/>
 </tile>
 <tile id="74">
  <image width="128" height="128" source="../animation/images/tiles/dirtCliff_left.png"/>
 </tile>
 <tile id="75">
  <image width="128" height="128" source="../animation/images/tiles/dirtCliff_right.png"/>
 </tile>
 <tile id="76">
  <image width="128" height="128" source="../animation/images/tiles/dirtCliffAlt_left.png"/>
 </tile>
 <tile id="77">
  <image width="128" height="128" source="../animation/images/tiles/dirtCliffAlt_right.png"/>
 </tile>
 <tile id="78">
  <image width="128" height="128" source="../animation/images/tiles/dirtCorner_left.png"/>
 </tile>
 <tile id="79">
  <image width="128" height="128" source="../animation/images/tiles/dirtCorner_right.png"/>
 </tile>
 <tile id="80">
  <image width="128" height="128" source="../animation/images/tiles/dirtHalf.png"/>
 </tile>
 <tile id="81">
  <image width="128" height="128" source="../animation/images/tiles/dirtHalf_left.png"/>
 </tile>
 <tile id="82">
  <image width="128" height="128" source="../animation/images/tiles/dirtHalf_mid.png"/>
 </tile>
 <tile id="83">
  <image width="128" height="128" source="../animation/images/tiles/dirtHalf_right.png"/>
 </tile>
 <tile id="84">
  <image width="128" height="128" source="../animation/images/tiles/dirtHill_left.png"/>
 </tile>
 <tile id="85">
  <image width="128" height="128" source="../animation/images/tiles/dirtHill_right.png"/>
 </tile>
 <tile id="86">
  <image width="128" height="128" source="../animation/images/tiles/dirtLeft.png"/>
 </tile>
 <tile id="87">
  <image width="128" height="128" source="../animation/images/tiles/dirtMid.png"/>
 </tile>
 <tile id="88">
  <image width="128" height="128" source="../animation/images/tiles/dirtRight.png"/>
 </tile>
 <tile id="89">
  <image width="128" height="128" source="../animation/images/tiles/doorClosed_mid.png"/>
 </tile>
 <tile id="90">
  <image width="128" height="128" source="../animation/images/tiles/doorClosed_top.png"/>
 </tile>
 <tile id="91">
  <image width="128" height="128" source="../animation/images/tiles/grass.png"/>
 </tile>
 <tile id="92">
  <image width="128" height="128" source="../animation/images/tiles/grass_sprout.png"/>
 </tile>
 <tile id="93">
  <image width="128" height="128" source="../animation/images/tiles/grassCenter.png"/>
 </tile>
 <tile id="94">
  <image width="128" height="128" source="../animation/images/tiles/grassCenter_round.png"/>
 </tile>
 <tile id="95">
  <image width="128" height="128" source="../animation/images/tiles/grassCliff_left.png"/>
 </tile>
 <tile id="96">
  <image width="128" height="128" source="../animation/images/tiles/grassCliff_right.png"/>
 </tile>
 <tile id="97">
  <image width="128" height="128" source="../animation/images/tiles/grassCliffAlt_left.png"/>
 </tile>
 <tile id="98">
  <image width="128" height="128" source="../animation/images/tiles/grassCliffAlt_right.png"/>
 </tile>
 <tile id="99">
  <image width="128" height="128" source="../animation/images/tiles/grassCorner_left.png"/>
 </tile>
 <tile id="100">
  <image width="128" height="128" source="../animation/images/tiles/grassCorner_right.png"/>
 </tile>
 <tile id="101">
  <image width="128" height="128" source="../animation/images/tiles/grassHalf.png"/>
 </tile>
 <tile id="102">
  <image width="128" height="128" source="../animation/images/tiles/grassHalf_left.png"/>
 </tile>
 <tile id="103">
  <image width="128" height="128" source="../animation/images/tiles/grassHalf_mid.png"/>
 </tile>
 <tile id="104">
  <image width="128" height="128" source="../animation/images/tiles/grassHalf_right.png"/>
 </tile>
 <tile id="105">
  <image width="128" height="128" source="../animation/images/tiles/grassHill_left.png"/>
 </tile>
 <tile id="106">
  <image width="128" height="128" source="../animation/images/tiles/grassHill_right.png"/>
 </tile>
 <tile id="107">
  <image width="128" height="128" source="../animation/images/tiles/grassLeft.png"/>
 </tile>
 <tile id="108">
  <image width="128" height="128" source="../animation/images/tiles/grassMid.png"/>
 </tile>
 <tile id="109">
  <image width="128" height="128" source="../animation/images/tiles/grassRight.png"/>
 </tile>
 <tile id="110">
  <image width="128" height="128" source="../animation/images/tiles/ladderMid.png"/>
 </tile>
 <tile id="111">
  <image width="128" height="128" source="../animation/images/tiles/ladderTop.png"/>
 </tile>
 <tile id="112">
  <image width="128" height="128" source="../animation/images/tiles/lava.png"/>
 </tile>
 <tile id="113">
  <image width="128" height="128" source="../animation/images/tiles/lavaTop_high.png"/>
 </tile>
 <tile id="114">
  <image width="128" height="128" source="../animation/images/tiles/lavaTop_low.png"/>
 </tile>
 <tile id="115">
  <image width="128" height="128" source="../animation/images/tiles/leverLeft.png"/>
 </tile>
 <tile id="116">
  <image width="128" height="128" source="../animation/images/tiles/leverMid.png"/>
 </tile>
 <tile id="117">
  <image width="128" height="128" source="../animation/images/tiles/leverRight.png"/>
 </tile>
 <tile id="118">
  <image width="128" height="128" source="../animation/images/tiles/lockRed.png"/>
 </tile>
 <tile id="119">
  <image width="128" height="128" source="../animation/images/tiles/lockYellow.png"/>
 </tile>
 <tile id="120">
  <image width="128" height="128" source="../animation/images/tiles/mushroomRed.png"/>
 </tile>
 <tile id="121">
  <image width="128" height="128" source="../animation/images/tiles/planet.png"/>
 </tile>
 <tile id="122">
  <image width="128" height="128" source="../animation/images/tiles/planetCenter.png"/>
 </tile>
 <tile id="123">
  <image width="128" height="128" source="../animation/images/tiles/planetCenter_rounded.png"/>
 </tile>
 <tile id="124">
  <image width="128" height="128" source="../animation/images/tiles/planetCliff_left.png"/>
 </tile>
 <tile id="125">
  <image width="128" height="128" source="../animation/images/tiles/planetCliff_right.png"/>
 </tile>
 <tile id="126">
  <image width="128" height="128" source="../animation/images/tiles/planetCliffAlt_left.png"/>
 </tile>
 <tile id="127">
  <image width="128" height="128" source="../animation/images/tiles/planetCliffAlt_right.png"/>
 </tile>
 <tile id="128">
  <image width="128" height="128" source="../animation/images/tiles/planetCorner_left.png"/>
 </tile>
 <tile id="129">
  <image width="128" height="128" source="../animation/images/tiles/planetCorner_right.png"/>
 </tile>
 <tile id="130">
  <image width="128" height="128" source="../animation/images/tiles/planetHalf.png"/>
 </tile>
 <tile id="131">
  <image width="128" height="128" source="../animation/images/tiles/planetHalf_left.png"/>
 </tile>
 <tile id="132">
  <image width="128" height="128" source="../animation/images/tiles/planetHalf_mid.png"/>
 </tile>
 <tile id="133">
  <image width="128" height="128" source="../animation/images/tiles/planetHalf_right.png"/>
 </tile>
 <tile id="134">
  <image width="128" height="128" source="../animation/images/tiles/planetHill_left.png"/>
 </tile>
 <tile id="135">
  <image width="128" height="128" source="../animation/images/tiles/planetHill_right.png"/>
 </tile>
 <tile id="136">
  <image width="128" height="128" source="../animation/images/tiles/planetLeft.png"/>
 </tile>
 <tile id="137">
  <image width="128" height="128" source="../animation/images/tiles/planetMid.png"/>
 </tile>
 <tile id="138">
  <image width="128" height="128" source="../animation/images/tiles/planetRight.png"/>
 </tile>
 <tile id="139">
  <image width="128" height="128" source="../animation/images/tiles/plantPurple.png"/>
 </tile>
 <tile id="140">
  <image width="128" height="128" source="../animation/images/tiles/rock.png"/>
 </tile>
 <tile id="141">
  <image width="128" height="128" source="../animation/images/tiles/sand.png"/>
 </tile>
 <tile id="142">
  <image width="128" height="128" source="../animation/images/tiles/sandCenter.png"/>
 </tile>
 <tile id="143">
  <image width="128" height="128" source="../animation/images/tiles/sandCenter_rounded.png"/>
 </tile>
 <tile id="144">
  <image width="128" height="128" source="../animation/images/tiles/sandCliff_left.png"/>
 </tile>
 <tile id="145">
  <image width="128" height="128" source="../animation/images/tiles/sandCliff_right.png"/>
 </tile>
 <tile id="146">
  <image width="128" height="128" source="../animation/images/tiles/sandCliffAlt_left.png"/>
 </tile>
 <tile id="147">
  <image width="128" height="128" source="../animation/images/tiles/sandCliffAlt_right.png"/>
 </tile>
 <tile id="148">
  <image width="128" height="128" source="../animation/images/tiles/sandCorner_left.png"/>
 </tile>
 <tile id="149">
  <image width="128" height="128" source="../animation/images/tiles/sandCorner_right.png"/>
 </tile>
 <tile id="150">
  <image width="128" height="128" source="../animation/images/tiles/sandHalf.png"/>
 </tile>
 <tile id="151">
  <image width="128" height="128" source="../animation/images/tiles/sandHalf_left.png"/>
 </tile>
 <tile id="152">
  <image width="128" height="128" source="../animation/images/tiles/sandHalf_mid.png"/>
 </tile>
 <tile id="153">
  <image width="128" height="128" source="../animation/images/tiles/sandHalf_right.png"/>
 </tile>
 <tile id="154">
  <image width="128" height="128" source="../animation/images/tiles/sandHill_left.png"/>
 </tile>
 <tile id="155">
  <image width="128" height="128" source="../animation/images/tiles/sandHill_right.png"/>
 </tile>
 <tile id="156">
  <image width="128" height="128" source="../animation/images/tiles/sandLeft.png"/>
 </tile>
 <tile id="157">
  <image width="128" height="128" source="../animation/images/tiles/sandMid.png"/>
 </tile>
 <tile id="158">
  <image width="128" height="128" source="../animation/images/tiles/sandRight.png"/>
 </tile>
 <tile id="159">
  <image width="128" height="128" source="../animation/images/tiles/signExit.png"/>
 </tile>
 <tile id="160">
  <image width="128" height="128" source="../animation/images/tiles/signLeft.png"/>
 </tile>
 <tile id="161">
  <image width="128" height="128" source="../animation/images/tiles/signRight.png"/>
 </tile>
 <tile id="162">
  <image width="128" height="128" source="../animation/images/tiles/snow.png"/>
 </tile>
 <tile id="163">
  <image width="128" height="128" source="../animation/images/tiles/snow_pile.png"/>
 </tile>
 <tile id="164">
  <image width="128" height="128" source="../animation/images/tiles/snowCenter.png"/>
 </tile>
 <tile id="165">
  <image width="128" height="128" source="../animation/images/tiles/snowCenter_rounded.png"/>
 </tile>
 <tile id="166">
  <image width="128" height="128" source="../animation/images/tiles/snowCliff_left.png"/>
 </tile>
 <tile id="167">
  <image width="128" height="128" source="../animation/images/tiles/snowCliff_right.png"/>
 </tile>
 <tile id="168">
  <image width="128" height="128" source="../animation/images/tiles/snowCliffAlt_left.png"/>
 </tile>
 <tile id="169">
  <image width="128" height="128" source="../animation/images/tiles/snowCliffAlt_right.png"/>
 </tile>
 <tile id="170">
  <image width="128" height="128" source="../animation/images/tiles/snowCorner_left.png"/>
 </tile>
 <tile id="171">
  <image width="128" height="128" source="../animation/images/tiles/snowCorner_right.png"/>
 </tile>
 <tile id="172">
  <image width="128" height="128" source="../animation/images/tiles/snowHalf.png"/>
 </tile>
 <tile id="173">
  <image width="128" height="128" source="../animation/images/tiles/snowHalf_left.png"/>
 </tile>
 <tile id="174">
  <image width="128" height="128" source="../animation/images/tiles/snowHalf_mid.png"/>
 </tile>
 <tile id="175">
  <image width="128" height="128" source="../animation/images/tiles/snowHalf_right.png"/>
 </tile>
 <tile id="176">
  <image width="128" height="128" source="../animation/images/tiles/snowHill_left.png"/>
 </tile>
 <tile id="177">
  <image width="128" height="128" source="../animation/images/tiles/snowHill_right.png"/>
 </tile>
 <tile id="178">
  <image width="128" height="128" source="../animation/images/tiles/snowLeft.png"/>
 </tile>
 <tile id="179">
  <image width="128" height="128" source="../animation/images/tiles/snowMid.png"/>
 </tile>
 <tile id="180">
  <image width="128" height="128" source="../animation/images/tiles/snowRight.png"/>
 </tile>
 <tile id="181">
  <image width="128" height="128" source="../animation/images/tiles/spikes.png"/>
 </tile>
 <tile id="182">
  <image width="128" height="128" source="../animation/images/tiles/stone.png"/>
 </tile>
 <tile id="183">
  <image width="128" height="128" source="../animation/images/tiles/stoneCenter.png"/>
 </tile>
 <tile id="184">
  <image width="128" height="128" source="../animation/images/tiles/stoneCenter_rounded.png"/>
 </tile>
 <tile id="185">
  <image width="128" height="128" source="../animation/images/tiles/stoneCliff_left.png"/>
 </tile>
 <tile id="186">
  <image width="128" height="128" source="../animation/images/tiles/stoneCliff_right.png"/>
 </tile>
 <tile id="187">
  <image width="128" height="128" source="../animation/images/tiles/stoneCliffAlt_left.png"/>
 </tile>
 <tile id="188">
  <image width="128" height="128" source="../animation/images/tiles/stoneCliffAlt_right.png"/>
 </tile>
 <tile id="189">
  <image width="128" height="128" source="../animation/images/tiles/stoneCorner_left.png"/>
 </tile>
 <tile id="190">
  <image width="128" height="128" source="../animation/images/tiles/stoneCorner_right.png"/>
 </tile>
 <tile id="191">
  <image width="128" height="128" source="../animation/images/tiles/stoneHalf.png"/>
 </tile>
 <tile id="192">
  <image width="128" height="128" source="../animation/images/tiles/stoneHalf_left.png"/>
 </tile>
 <tile id="193">
  <image width="128" height="128" source="../animation/images/tiles/stoneHalf_mid.png"/>
 </tile>
 <tile id="194">
  <image width="128" height="128" source="../animation/images/tiles/stoneHalf_right.png"/>
 </tile>
 <tile id="195">
  <image width="128" height="128" source="../animation/images/tiles/stoneHill_left.png"/>
 </tile>
 <tile id="196">
  <image width="128" height="128" source="../animation/images/tiles/stoneHill_right.png"/>
 </tile>
 <tile id="197">
  <image width="128" height="128" source="../animation/images/tiles/stoneLeft.png"/>
 </tile>
 <tile id="198">
  <image width="128" height="128" source="../animation/images/tiles/stoneMid.png"/>
 </tile>
 <tile id="199">
  <image width="128" height="128" source="../animation/images/tiles/stoneRight.png"/>
 </tile>
 <tile id="200">
  <image width="128" height="128" source="../animation/images/tiles/switchGreen.png"/>
 </tile>
 <tile id="201">
  <image width="128" height="128" source="../animation/images/tiles/switchGreen_pressed.png"/>
 </tile>
 <tile id="202">
  <image width="128" height="128" source="../animation/images/tiles/switchRed.png"/>
 </tile>
 <tile id="203">
  <image width="128" height="128" source="../animation/images/tiles/switchRed_pressed.png"/>
 </tile>
 <tile id="204">
  <image width="128" height="128" source="../animation/images/tiles/torch1.png"/>
 </tile>
 <tile id="205">
  <image width="128" height="128" source="../animation/images/tiles/torch2.png"/>
 </tile>
 <tile id="206">
  <image width="128" height="128" source="../animation/images/tiles/torchOff.png"/>
 </tile>
 <tile id="207">
  <image width="128" height="128" source="../animation/images/tiles/water.png"/>
 </tile>
 <tile id="208">
  <image width="128" height="128" source="../animation/images/tiles/waterTop_high.png"/>
 </tile>
 <tile id="209">
  <image width="128" height="128" source="../animation/images/tiles/waterTop_low.png"/>
 </tile>
</tileset>
