"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from cmath import exp
from sre_constants import JUMP
from settings_global import *
import arcade
import os

#DO NOT EDIT: Absolute path to the sound files directory
TEXTURE_PATH = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "examples")

#NOTE: EXAMPLE
def projectile_sound() -> None:
    projectile_sound = arcade.sound.load_sound(
        os.path.join(
            TEXTURE_PATH,
            "laser2.wav"
        )
    )
    arcade.sound.play_sound(projectile_sound)

def level_sound(
    game_time   : int,
    game_music  : arcade.Sound,
    media_player: arcade.pyglet.media.player.Player):

    #TODO:
    '''Change the game sound based on time reminaing
        Step 0) Review the function 'projectile_sound' for an example
        Step 1) Play music when the game_time is equal to 300
        Step 2) Speed up the game music when different times are hit!
            - Change the pitch
        Step 3) Create a IF condition with the variable: game_time
            - IF TRUE, then play sound
        Step 4) Change the pitch of the media_player object

        NOTE: Example IF condition
        if <x> == <a>:
            do_action

        NOTE: How to load a sound file
        my_sound = arcade.sound.load_sound(
            os.path.join(
                TEXTURE_PATH,
                "laser2.wav"
            )
    )

    '''

    #TODO TASK 1: Play game sound with game_time equals 300
    """The game duration is 300 seconds.
        - When should the music be played?
        Step 1) Create a if condition with game_time
            - When does the game start?
        Step 2) Create a music_sound variable inside the IF condition
            - This will load your sound file
            - Copy the example from projectile_sound()
        Step 3) Play the music and assign it to the media_player
            - meda_player = <my_music_variable>.play()
        Step 4) return the media_player object
    """
    if game_time == 300:
        my_sound = arcade.sound.load_sound(
            os.path.join(
            TEXTURE_PATH,
            "funkyrobot.mp3"))
        media_player = my_sound.play()
        # Great job! We need to return the media_player object
        return media_player

    #TODO TASK 2: When should the game music change?
    """You want to change: media_player.pitch
        Step 1) Create a if condition with the variable game_time
        Step 2) Change the media_player pitch
        Step 3) return the media_player object
    """
    return None
    
def damage_taken_sound() -> None:
    #TODO Task 3:
    '''Play a sound when damage is taken
        Step 1) Find a sound file to play
        Step 2) Get the arcade game to play the sound
    '''

    my_sound = arcade.sound.load_sound(
        os.path.join(
        TEXTURE_PATH,
        "hurt3.wav"))
    media_player = my_sound.play()
    # Great job! We need to return the media_player object
    return media_player
    #arcade.sound.play_sound()
    pass

def mob_destroyed_sound() -> None:
        #TODO Task 4:
    '''Play a sound when a mob is destroyed
        Step 1) Find a sound file to play
        Step 2) Get the arcade game to play the sound
    '''
    #arcade.sound.play_sound()
    pass
    my_sound = arcade.sound.load_sound(
        os.path.join(
        TEXTURE_PATH,
        "explosion1.wav"))
    MOB_DAMAGE = my_sound.play()
    pass


def coin_collected_sound() -> None:
    #TODO Task 5:
    '''Play a sound when a coin is collected
        Step 1) Find a sound file to play
        Step 2) Get the arcade game to play the sound
    '''
    #arcade.sound.play_sound()
    pass
    my_sound = arcade.sound.load_sound(
        os.path.join(
        TEXTURE_PATH,
        "coin2.wav"))
    coin_collected_sound = my_sound.play()
def victory_sound() -> None:
    #TODO BONUS:
    '''Play a sound when the player wins
        Step 1) Find a sound file to play
        Step 2) Get the arcade game to play the sound
    '''
    my_sound = arcade.sound.load_sound(
        os.path.join(
        TEXTURE_PATH,
        "upgrade4.wav"))
    victory_sound = my_sound.play()
    #arcade.sound.play_sound()
    pass
    
    pass
    my_sound = arcade.sound.load_sound()
    os.path.join(
        TEXTURE_PATH,
        "phaseJump1.wav")
    JUMP_sound= my_sound.play()
    #can we add a jump sound and falling sound 
    pass
    my_sound = arcade.sound.load_sound()
    os.path.join(
        TEXTURE_PATH,
        "rockHit2.wav")
    falling_sound= my_sound.play()