"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

# Python Libraries: DO NOT CHANGE
import arcade
import time
import os
import copy

# Game Modules: DO NOT CHANGE
from settings_global         import *
from player.player           import *
from respawn.respawns        import *
from map.levels              import *
from map.map        import *
from mobs.mobs              import *
from sound.sound             import *
from score.score             import get_score
from mobs.boss               import *
from collisions.collisions   import *
from animation.player1       import *
from animation.player2       import *

class MyGame(arcade.Window):
    """Main application/game app
    """

    def __init__(self,
        window_height: int,
        window_width : int,
        window_title : str) -> None:
        """Setup game window and settings
        DO NOT MODIFY THIS UNLESS YOU HAVE A PLAN!!!! :)

        :param window_height: Height of game window
        :type window_height : int
        :param window_width : Width of game window
        :type window_width  : int
        :param window_title : Name of window title
        :type window_title  : str
        """
        # Super lets our class MyGame inherit arcade.Windows methods
        # Inherit means use or share
        super().__init__(window_width, window_height, window_title)

        # Setup object lists for game window
        self.game_objects = {}

        # Spawn locations for mobs
        self.mob_spawn_locations = []
        self.mob_collision_wall_sprites = arcade.SpriteList()

        # Player/Level class objects
        self.Player = None
        self.Level  = None

        # Music object
        self.my_music        = None
        self.my_media_player = None

        # Cameras
        self.camera     = None
        self.gui_camera = None
        self.check_point_flag = False

        # Current Level & score
        self.current_level = None
        self.current_score = 0

        # TODO
        self.moving_tile_1_position_index  = 0
        self.moving_tile_2_position_index  = 0

        # Texture paths
        self.sprite_textures = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "animation",
            "sprites")

    def setup(self):
        """Set all game variables: player, mobs, maps, etc....

        DO NOT MODIFY THIS UNLESSS YOU HAVE A PLAN!! :)
        """

        # Create player
        # Default player is created
        default_sprite_png = ":resources:images/animated_characters/male_person/malePerson_idle.png"
        default_spawn_xy   = (100, 100)

        new_player = self.create_default_player(
            default_sprite_png,
            default_spawn_xy)

        # Animated player
        texture_path = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "animation",
            "images",
            "players"
        )

        if PLAYER_MODEL == 0:
            new_animated_player = create_animated_player1(texture_path)
        else:
            new_animated_player = create_animated_player2(texture_path)
        if new_animated_player != None:
            new_animated_player.is_animated    = True
            new_animated_player.animated_index = 0
            new_animated_player.key            = None
            self.game_objects['player']        = new_animated_player
        else:
            new_player.is_animated      = False
            self.game_objects['player'] = new_player

        # Set player Stats
        self.game_objects['player'].hp     = MAX_HEALTH
        self.game_objects['player'].damage = DAMAGE

        # Create boss and set stats
        self.game_objects['boss']        = create_zombie_level_boss(self.sprite_textures)
        self.game_objects['boss'].hp     = BOSS_HP
        self.game_objects['boss'].damage = BOSS_DMG

        self.game_objects['boss'].path_index = 0

        # Create map/level
        self.Level         = Levels()
        self.current_level = self.Level.build_level_1(
            self.game_objects['player'],
            self.game_objects['boss'])

        # At the start: remaing time = max time duration
        self.time_remaining = self.current_level.time_limit
        self.start_time = time.time()
        self.game_over  = False

        # Assign player HP
        self.current_health = MAX_HEALTH

        # Assign all other game objects
        self.reset_game_level()

        # Init Camera settings
        self.camera     = arcade.Camera(self.width, self.height)
        self.gui_camera = arcade.Camera(self.width, self.height)
        self.pan_camera_to_user()

        # Set any texture paths ahead of time
        self.projectile_texture_path = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "animation",
            "images",
            "projectiles")
        arcade.set_background_color(arcade.color.AMAZON)

    def reset_game_level(self):
        """
        """
        # Player settings
        self.game_objects['player'].powerup = False

        # Explosions
        self.explosions_list = arcade.SpriteList()

        # Physical barriers
        self.game_objects['walls']      = self.current_level.wall_list
        self.game_objects['goals']      = self.current_level.goals
        self.game_objects['mob_ground'] = self.current_level.mob_ground
        self.game_objects['goal']       = self.current_level.goals

        # Mobs
        self.game_objects['mobs']        = self.current_level.mobs
        self.game_objects['flying_mobs'] = self.current_level.flying_mobs

        # Interactive map items
        self.game_objects['coins']         = self.current_level.coin_list
        self.game_objects['death_traps']   = self.current_level.death_traps
        self.game_objects['powerups']      = self.current_level.powerups
        self.game_objects['check_points']  = self.current_level.check_points

        # Get the origin position for the moving tiles
        self.moving_tiles_origin = []
        for mv_tile in self.current_level.moving_tiles:
            self.moving_tiles_origin.append(
                (mv_tile.center_x, mv_tile.center_y))

        self.game_objects['moving_tiles']  = self.current_level.moving_tiles
        self.game_objects['moving_tile_1'] = self.current_level.moving_tiles[:2]
        self.game_objects['moving_tile_2'] = self.current_level.moving_tiles[2:]

        # Set mob respawns
        self.mob_spawn_locations = []
        for mob in self.game_objects['mobs']:
            self.mob_spawn_locations.append((mob.center_x, mob.center_y))

        # Projectiles
        self.game_objects['projectiles'] = []
        self.projectile_remaining        = PROJECTILE_COUNT

        # Respawn player and reset stats
        if self.game_over:
            (x, y) = player_respawn_location(
                self.check_point_flag,
                self.game_objects['player'].center_x,
                self.game_objects['player'].center_y)
            self.game_objects['player'].center_x = x
            self.game_objects['player'].center_y = y
            self.game_objects['player'].damage   = DAMAGE
            self.game_objects['player'].hp       = MAX_HEALTH

            # Player game settings in game info
            self.current_score  = 0
            self.time_remaining = self.current_level.time_limit

    def on_draw(self) -> None:
        """Update visuals on screen
        Loop through object lists and draw each one
        """

        # Reset camera/map
        self.camera.use()
        self.clear()

        # Explosion Effects
        self.explosions_list.draw()

        # Loop through each game object (i.e., coins, mobs, ...)
        # Draw each one! If you don't draw it, then you can't see it!!
        # object_type is a key inside self.game_objects
        # Each key has a value that it returns, like unlocking a chest
        for object_type in self.game_objects:
            if object_type in ["player", "boss"]:
                self.game_objects[object_type].draw()
                continue
            for game_sprite in self.game_objects[object_type]:
                game_sprite.draw()

        # Update gui camera after all objects are loaded
        self.gui_camera.use()

        # UPDATE SCORE
        get_score(
            self.current_score,
            None,
            self.game_objects['player'])

        score_info = f"Score: {self.current_score}"

        # UPDATE TIME REMAINING
        import time
        current_time   = time.time()
        self.time_remaining  = self.current_level.time_limit
        self.time_remaining -= int(current_time -self.start_time)

        result = level_sound(self.time_remaining, self.my_music, self.my_media_player)

        if result != None:
            self.my_media_player = result

        player_info  = f"Health: {self.game_objects['player'].hp}"
        player_info += f"  Time Left: {self.time_remaining}"

        # DRAW INFO
        arcade.draw_text(
            player_info,
            10, self.height-20, arcade.color.WHITE, 16)

        # DRAW INFO
        arcade.draw_text(
            score_info,
            self.width-200,self.height-20, arcade.color.WHITE, 16)

        # DRAW INFO
        projectile_info = f"Lazers: {self.projectile_remaining}"
        arcade.draw_text(
            projectile_info,
            10,self.height-40, arcade.color.WHITE, 16)

    def create_default_player(self,
        sprite_png: str,
        spawn_xy  : Tuple[int, int]) -> arcade.Sprite:
        """Load player sprite image and action sprites

        :returns player: Player selft object
        :rtpe: Player
        """

        # Player
        self.player_sprite = arcade.Sprite(sprite_png, 0.5)

        # Player starting location on the map
        self.player_sprite.center_x = spawn_xy[0] # int: [0,width]
        self.player_sprite.center_y = spawn_xy[1] # int: [0,height]

        absolute_path_to_map = os.path.dirname(os.path.realpath(__file__))

        return self.player_sprite

    def create_boss_sprite(self) -> arcade.Sprite:
        """Load player sprite image and action sprites

        :returns player: Player selft object
        :rtpe: Player
        """

        # Boss sprite image
        sprite_png = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "animation",
            "sprites",
            "zombie_idle.png"
        )

        # Player
        self.boss_sprite = arcade.Sprite(sprite_png, 1.5)

        # Player starting location on the map
        self.boss_sprite.center_x = 4576 # int: [0,width]
        self.boss_sprite.center_y = 300 # int: [0,height]

        absolute_path_to_map = os.path.dirname(os.path.realpath(__file__))

        return self.boss_sprite

    def on_key_press(self, key, modifiers):
        """
        Called whenever a key is pressed.
        """
        player = self.game_objects['player']

        player.key = key
        if key == arcade.key.SPACE:
            if self.Level.physics_engine.can_jump():
                player.change_y = JUMP_SPEED
        elif key == arcade.key.A:
            player.change_x = -MOVEMENT_SPEED
        elif key == arcade.key.D:
            player.change_x = MOVEMENT_SPEED

    def on_key_release(self, key, modifiers):
        """
        Called when the user presses a mouse button.
        """
        player = self.game_objects['player']

        if key == arcade.key.A or key == arcade.key.D:
            player.key = None
            player.change_x = 0

    def on_mouse_press(self, x: float, y: float, button, modifiers) -> None:
        """Capture all mouse left/right click events
        DO NOT MODIFY UNLESS YOU HAVE A PLAN!!! :)
        """

        player = self.game_objects['player']

        x1 = player.center_x
        y1 = player.center_y

        print(f"DEBUG INFO\n: Player (x,y) == {(x1, y1)}")
        print(f"MOUSE (x,y) == {(x,y)}")

        # The mouse is bound to the original window width/height
        # Update mouse x when the user moves past width/2
        if x1 > self.width/2:
            x += x1-self.width/2

        # x & y is the mouse click location = x2 & y2
        if self.projectile_remaining > 0:
            self.game_objects['projectiles'].append(
                create_projectile(x1, y1, x, y, self.projectile_texture_path))

            # Projectile sound
            projectile_sound()

            # Reduce the projectiles reminaing
            self.projectile_remaining -= 1

    def on_update(self, delta_time: float):
        """Movement and game logic TBA
        """

        # Update physics engine
        # This will update all objects attached to the level

        self.Level.physics_engine.update()
        self.Level.boss_physics_engine.update()

        for mob_physics in self.Level.mob_physics:
            mob_physics.update()

        # Explosion List
        self.explosions_list.update()

        # Update player
        self.game_objects['player'].update()
        self.game_objects['player'].update_animation()

        # Update boss
        self.game_objects['boss'].update()
        self.game_objects['boss'].update_animation()

        # Update all other game objects
        # animated objects have one extra update
        player_checkpoint_collisions(self)

        # Loop through all existing projectiles till the animation is over
        projectile_collisions(self)

        # Check for coin collisions with player
        player_coin_collisions(self)

        # Check if player got the powerup
        player_powerup_collisions(self)

        # Update First moving tile
        # self.moving_tile_position_index
        # TODO: BUG FIX IN ORDERING
        self.moving_tile_1_position_index = second_moving_tile(
            self.game_objects['moving_tile_1'][0],
            self.moving_tile_1_position_index
        )
        self.game_objects['moving_tile_1'][1].change_x = self.game_objects['moving_tile_1'][0].change_x
        self.game_objects['moving_tile_1'][1].change_y = self.game_objects['moving_tile_1'][0].change_y

        self.moving_tile_2_position_index = first_moving_tile(
            self.game_objects['moving_tile_2'][0],
            self.moving_tile_2_position_index
        )
        self.game_objects['moving_tile_2'][1].change_x = self.game_objects['moving_tile_2'][0].change_x

        # Check for mob collisions with player
        player_mob_collisions(self)

        # Check if player lost the game
        player = self.game_objects['player']
        self.game_over = player_respawn_conditions(
            player.hp,
            player.center_x,
            player.center_y,
            self.time_remaining,
            self.check_point_flag
        )

        if self.game_over:
            # Respawn the player and all mobs
            self.reset_game_level()
            self.game_over = False


        # Update BOSS movement
        zombie_boss_movement(
            self.game_objects['boss'],
            player)

        # Pan camera to user as they move, but slowly
        self.pan_camera_to_user(panning_fraction=0.12)

    def pan_camera_to_user(self, panning_fraction: float = 1.0) -> None:
        """ Manage Scrolling
        """

        # Center the camera on each player
        player = self.game_objects['player']

        screen_center_x = player.center_x - (self.camera.viewport_width / 2)
        screen_center_y = player.center_y - (
            self.camera.viewport_height / 2)

        if screen_center_x < 0:
            screen_center_x = 0
        if screen_center_y < 0:
            screen_center_y = 0
        user_centered = screen_center_x, screen_center_y

        self.camera.move_to(user_centered, panning_fraction)

def main():
    game = MyGame(GAME_HEIGHT, GAME_WIDTH, "Level 1 Alien Hero!!")
    game.setup()
    arcade.run()


if __name__ == "__main__":
    main()
