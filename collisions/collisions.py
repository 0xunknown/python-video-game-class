"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import arcade
from sound.sound          import *
from score.score          import *
from mobs.mobs            import *
from animation.Explosions import *


def player_checkpoint_collisions(game_object) -> None:
    """
        Paramters:
            game_object (MyGame): Class object from game.p
            - Holds all information about the overall game
    """
    for object_type in game_object.game_objects:
        if object_type in ["player", "boss"]:
            continue
        for game_sprite in game_object.game_objects[object_type]:
            game_sprite.update()
            game_sprite.update_animation()

    # See if the player reached the flag!
    hit_list = arcade.check_for_collision_with_list(
        game_object.game_objects['player'],
        game_object.game_objects['check_points']
    )

    if len(hit_list) > 0:
        game_object.check_point_flag = True
        game_object.current_score = get_score(
            game_object.current_score,
            "check_point",
            game_object.game_objects['player'])

def projectile_collisions(game_object):
    """
        Paramters:
            game_object (MyGame): Class object from game.p
            - Holds all information about the overall game
    """
    player = game_object.game_objects['player']
    for projectile in game_object.game_objects['projectiles']:
        # Check for collision with boss
        boss_hit = arcade.check_for_collision(
            projectile,
            game_object.game_objects['boss'])

        if boss_hit:
            projectile.remove_from_sprite_lists()
            if "invalid" not in dir(projectile):
                game_object.game_objects['boss'].hp -= DAMAGE
                if game_object.game_objects['boss'].hp < 0:
                    game_object.game_objects['boss'].remove_from_sprite_lists()
                    game_object.current_score = get_score(
                        game_object.current_score,
                        "boss_destroyed",
                        player)
                    game_object.game_objects['boss'].kill()
                projectile.invalid = True
                create_mob_explosions(
                    "boss",
                    game_object.game_objects['boss'].center_x,
                    game_object.game_objects['boss'].center_y,
                    game_object
                )
            projectile.kill()

        # Check for collisions with other sprites
        hit_list = arcade.check_for_collision_with_list(
            projectile,
            game_object.game_objects['mobs'])

        # Remove projectile if contact is made
        # Remove mob that made contact
        if len(hit_list) > 0:
            if "invalid" not in dir(projectile):
                mob_destroyed_sound()
                projectile.remove_from_sprite_lists()
                hit_list[0].remove_from_sprite_lists()
                game_object.current_score = get_score(
                    game_object.current_score,
                    "mob_destroyed",
                    player)
                projectile.invalid = True
                create_mob_explosions(
                    "normal",
                    hit_list[0].center_x,
                    hit_list[0].center_y,
                    game_object
                )
                projectile.kill()

        # Remove projectile if contact is made with a wall/ground/etc
        hit_list = arcade.check_for_collision_with_list(
            projectile,
            game_object.game_objects['walls'])

        if len(hit_list) > 0:
            projectile.remove_from_sprite_lists()
            projectile.kill()

        # Remove projectile if contact is made with a goal
        hit_list = arcade.check_for_collision_with_list(
            projectile,
            game_object.game_objects['goals'])

        if len(hit_list) > 0:
            projectile.remove_from_sprite_lists()

        # Remove projectile if contact is made with a moving tile
        hit_list = arcade.check_for_collision_with_list(
            projectile,
            game_object.game_objects['moving_tiles'])

        if len(hit_list) > 0:
            projectile.remove_from_sprite_lists()

        # Remove bullet if it flies off-screen
        if projectile.top > game_object.camera.viewport_height:
            projectile.remove_from_sprite_lists()

def player_coin_collisions(game_object):
    """
        Paramters:
            game_object (MyGame): Class object from game.p
            - Holds all information about the overall game
    """
    player = game_object.game_objects['player']
    coins_hit = arcade.check_for_collision_with_list(
        player,
        game_object.game_objects['coins'])

    for coin in coins_hit:
        coin_collected_sound()
        coin.remove_from_sprite_lists()
        game_object.current_score = get_score(
            game_object.current_score,
            "coin",
            player)

def player_powerup_collisions(game_object):
    """
        Paramters:
            game_object (MyGame): Class object from game.p
            - Holds all information about the overall game
    """
    player = game_object.game_objects['player']
    # Check if player got the powerup
    powerup_hit = arcade.check_for_collision_with_list(
        player,
        game_object.game_objects['powerups']
    )

    for powerup in powerup_hit:
        powerup.remove_from_sprite_lists()
        player.powerup = True

def player_mob_collisions(game_object):
    """
        Paramters:
            game_object (MyGame): Class object from game.p
            - Holds all information about the overall game
    """
    player = game_object.game_objects['player']
    mobs_hit = arcade.check_for_collision_with_list(
        player,
        game_object.game_objects['mobs'])

    for mob in mobs_hit:
        # Decrease players HP
        player.hp -= MOB_DAMAGE
        damage_taken_sound()
        player.center_x -= MOB_X_PUSHBACK
        player.center_y += MOB_Y_PUSHBACK

    boss_hit = arcade.check_for_collision(
        player,
        game_object.game_objects['boss'])

    if boss_hit:
        if game_object.game_objects['boss'].hp > 0:
            player.hp -= BOSS_DMG
            damage_taken_sound()
            player.center_x -= BOSS_X_PUSHBACK
            player.center_y += BOSS_Y_PUSHBACK

    # Update the mobs position
    for i in range(0, len(game_object.game_objects['mobs'])):
        mob = game_object.game_objects['mobs'][i]
        mob_collisions = arcade.check_for_collision_with_list(
            mob,
            game_object.game_objects['mob_ground']
        )

        collision_detected = False
        if len(mob_collisions) > 0:
            game_object.game_objects['mobs'][i].collision ^= True
            collision_detected = True

        update_sprite_movement(
            mob,
            player,
            game_object.game_objects['mobs'][i].collision)
