"""Copyright 2022 Donald Beyette and Michael Rugh

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import arcade
class Explosion(arcade.Sprite):
    """ This class creates an explosion animation """

    def __init__(self, texture_list):
        super().__init__()

        # Start at the first frame
        self.current_texture = 0
        self.textures = texture_list

    def update(self):

        # Update to the next frame of the animation. If we are at the end
        # of our frames, then delete this sprite.
        self.current_texture += 1
        if self.current_texture < len(self.textures):
            self.set_texture(self.current_texture)
        else:
            self.remove_from_sprite_lists()

# BONUS
def create_mob_explosions(
    mob_type   : str,
    mob_x      : int,
    mob_y      : int,
    game_object) -> None:
    """Create a explosion effect where the mob was destroyed

        Params:
            mob_type (string)   : type of mob normal or boss
            mob_x    (int)      : mob x location
            mob_y    (int)      : mob y location
            game_object (MyGame): game.py class object MyGame
    """

    # Set of PNGs for the explosion animation
    explosion_texture_list = []

    # Bonus TODO: Explosion PNG Settings
    """
        Step 1) Find the explosion PNG
            - animation/images/spritesheets/explosion.png
        Step 2) What kind of png is this?
        Step 3) How many columns are there?
        Step 4) How many animations do you want (count)
        Step 5) What should the width/height be of ot he image?
    """
    columns       = 0
    count         = 0
    sprite_width  = 0
    sprite_height = 0
    file_name = "animation/images/spritesheets/explosion.png"

    # DO NOT CHANGE: Load the explosions from a sprite sheet
    explosion_texture_list = arcade.load_spritesheet(
        file_name,
        sprite_width,
        sprite_height,
        columns,
        count)

    # DO NOT CHANGE
    explosion = Explosion(explosion_texture_list)

    # BONUS: What is the correct x and y value?
    explosion.center_x = 0
    explosion.center_y = 0

    # DO NOT CHANGE: Call update()
    explosion.update()

    # DO NOT CHANGE: Add to a list of sprites that are explosions
    game_object.explosions_list.append(explosion)